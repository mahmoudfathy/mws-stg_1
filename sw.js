/*==============SW===============*/
// import { idb } from 'idb';
importScripts('/js/idb.js');
let version = '1.7.0';
let staticCacheName = 'mws-stage1' + version;
self.addEventListener('activate',  function (event) {
  event.waitUntil(self.clients.claim());
  //createIDB()
});

 
  var dbPromise = idb.open('restaurants-db', 1, function (upgradeDb){
    var keyValStore =  upgradeDb.createObjectStore('restaurants', { keyPath: 'id' });
  })
 

self.addEventListener('fetch', function(event) {
   if (event.request.url.indexOf('localhost:1337')>0){
     event.respondWith(
      dbPromise.then(function (db) {
        var tx = db.transaction('restaurants');
        var restaurntsStore = tx.objectStore('restaurants');
        return restaurntsStore.getAll();
      }).then(function (retaurnats) {
        console.log('myrestaurnats Are:', retaurnats);
        if (retaurnats.length>0)
        {
          // Found Data in DB
          console.log('event respond read from DB');
          let response = new Response(JSON.stringify(retaurnats), {
            headers: new Headers({
              'Content-type': 'application/json',
              'Access-Control-Allow-Credentials': 'true'
            }),
            type: 'cors',
            status: 200
          });
          return response;
        }
        else{
          //fetch from netwowk
          return fetch(event.request).then(function (response) {
            console.log('myResponse:>>>', response);
            return response.clone().json().then(json_response => {
              console.log('fetch from net');
              pushDataTODB(json_response);
              return response;
            })
          });
  
        }
      })
     )
        //do not go to the normal caching
        return;
   };
  event.respondWith(
    caches.open(staticCacheName).then(function(cache) {
      return cache.match(event.request).then(function(response) {
        var fetchPromise = fetch(event.request).then(function(networkResponse) {
        //I will not cach googl maps
          cache.put(event.request, networkResponse.clone());
          return networkResponse;
        })
        return response || fetchPromise;
      })
    })
  );
});

function pushDataTODB(restaurnatsData){
  console.log(restaurnatsData);
  dbPromise.then(function(db) {
   var tx = db.transaction('restaurants', 'readwrite');
    var store = tx.objectStore('restaurants');
    restaurnatsData.forEach(function(item) {
      store.put(item);  // put is safer because it doesn't give error on duplicate add
    });
    return tx.complete;
  }).then(function() {
    console.log('added Data to DB');
  }).catch(function(err) {
    console.log('error in DB adding', err);
  });

}

/*======= delete old cache======= */
self.addEventListener('activate', function(event) {
  var cacheWhitelist = [staticCacheName];
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (cacheWhitelist.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

